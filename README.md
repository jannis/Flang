# Flang

You can read the [Rules here](https://codeberg.org/jannis/Flang/wiki/Rules).

## Forum

There is a [forum page](https://flang.tadris.de/)! You can ask questions there, help other players understanding the game, publish your new opening or strategies that you discovered.

## Chat

Join the bridged chat on:

- Matrix: [#flang:matrix.org](https://matrix.to/#/#flang:matrix.org)
- Telegram: [@flanggame](https://t.me/flanggame)

## Repos and creating issues

Please take note that there are three repositories and issue trackers for different purposes:

- [Flang](https://codeberg.org/jannis/Flang/issues): about the game itself
- [Android](https://codeberg.org/jannis/FlangAndroid): the official Android client
- [Desktop](https://codeberg.org/jannis/FlangDesktop): Desktop client in development
- [Server](https://codeberg.org/jannis/FlangServer): the official Flang Server