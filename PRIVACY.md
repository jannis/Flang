# Privacy Policy

## Introduction

This Privacy Policy sets out how the Android app "Flang" uses and protects your personal data that you provide, or that is otherwise obtained or generated. It only applies if you are using the official Flang server which is used by default.

## Collected information

Flang is designed to collect as little personal information as possible. Collected information includes:

- IP addresses
- Authentication information

Additionally, the following data is stored in combination with your user account if you choose to create an account:

- Games played
- Online rating history
- Player statistics
- Chat messages

## How the data is used

The data is used to provide a working multiplayer platform. 

IP addresses and requests are saved for 30-90 days in the server logs and used for spam protection.

Games, rating, statistics and chat messages are saved in order to display them to you and other players.

## What data is shared

Personal information is NOT sold or given to anyone, with the following exceptions:

- Your public username, games and statistics are accessible to other Flang players.
- Your chat messages are accessible to other Flang players in the chat room.
- If a court order forces me to share data, I am forced to do so.

## Transparency

You can read through the source code of the Flang [Android client](https://codeberg.org/jannis/FlangAndroid) and the [server](https://codeberg.org/jannis/FlangServer).

# Keeping your data safe

Your data is stored on servers in Germany.
When your phone connects to our servers, it uses SSL-encrypted connections that none of the tranferring servers can read it.

# Deleting data

## Deletion by request

You can delete your account using [this link](https://home.tadris.de:9090/cardcontest/deleteAccount/index). If you have further questions please write an E-Mail to support@tadris.de including your username and demand. We will do our best to send you a reply within one week, but we cannot guarantee that. We will reply no later than 30 days after receipt of the message.

The deletion will instantly delete all data associated with the account including login and game data.